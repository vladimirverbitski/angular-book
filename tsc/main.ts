const hello = 'Hello';

enum Color {
    Red,
    Green,
    Blue
}

let c: Color = Color.Red;

function f(x, ...a) {
    let sum = x;
    for (let i = 0; i < a.length; ++i)
        sum += a[i];
    return sum;
}

const ar = [1,2,3,4].find( x => x % 2);

console.log(ar);