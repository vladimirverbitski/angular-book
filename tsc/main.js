var hello = 'Hello';
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
var c = Color.Red;
function f(x) {
    var a = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        a[_i - 1] = arguments[_i];
    }
    var sum = x;
    for (var i = 0; i < a.length; ++i)
        sum += a[i];
    return sum;
}
var ar = [1, 2, 3, 4].find(function (x) { return x % 2; });
console.log(ar);
