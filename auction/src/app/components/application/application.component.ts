import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/shared/interfaces/product';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {

  products: Array<Product> = [];

  constructor(
    private productService: ProductService
  ) {
    this.products = this.productService.getProducts();
   }

  ngOnInit(): void {
  }

}
